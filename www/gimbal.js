/*global cordova*/
cordova.define("cordova/plugin/gimbal",
    function (require, exports, module) {
        var exec = cordova.require('cordova/exec');

        module.exports = {
          configure: {
            enableLocalNotification: function(enabled) {
              exec(function(){}, function(){}, 'Gimbal', 'enableLocalNotification', [enabled]);
            },
            setLocalNotificationMessage: function(message) {
              exec(function(){}, function(){}, 'Gimbal', 'setLocalNotificationMessage', [message]);
            },
            setLocalNotificationRange: function(range) {
              exec(function(){}, function(){}, 'Gimbal', 'setLocalNotificationRange', [range]);
            },
            setLocalNotificationMaxSpam: function(delay) {
              exec(function(){}, function(){}, 'Gimbal', 'setLocalNotificationMaxSpam', [delay]);
            },
            setLocalNotificationCallback: function(callback) {
              exec(callback, function(){}, 'Gimbal', 'setLocalNotificationCallback', []);
            }
          },
          startService: function(appid, appsecret, callbackurl, success, failed) {
            exec(success, failed, 'Gimbal', 'startService', [appid, appsecret, callbackurl]);
          },
          didArriveMonitoring: function(success, failed) {
            exec(success, failed, 'Gimbal', 'didArriveMonitoring', []);
          },
          didDepartMonitoring: function(success, failed) {
            exec(success, failed, 'Gimbal', 'didDepartMonitoring', []);
          },
          didReceivedMonitoring: function(success, failed) {
            exec(success, failed, 'Gimbal', 'didReceivedMonitoring', []);
          }
        };
    });

