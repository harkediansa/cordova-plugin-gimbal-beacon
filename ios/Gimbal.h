//
//  Gimbal.h
//  Beacon
//
//  Created by Lionel Penaud on 02/10/2014.
//
//

#ifndef Beacon_gimbal_h
#define Beacon_gimbal_h

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

@interface Gimbal : CDVPlugin

- (void)enableLocalNotification:(CDVInvokedUrlCommand*)command;
- (void)setLocalNotificationMessage:(CDVInvokedUrlCommand*)command;
- (void)setLocalNotificationRange:(CDVInvokedUrlCommand*)command;
- (void)setLocalNotificationMaxSpam:(CDVInvokedUrlCommand*)command;
- (void)setLocalNotificationCallback:(CDVInvokedUrlCommand*)command;
- (void)startService:(CDVInvokedUrlCommand*)command;
- (void)didArriveMonitoring:(CDVInvokedUrlCommand*)command;
- (void)didDepartMonitoring:(CDVInvokedUrlCommand*)command;
- (void)didReceivedMonitoring:(CDVInvokedUrlCommand*)command;

@end


#endif

